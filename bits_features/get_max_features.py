import pandas as pd
import numpy as np
import os
from os import system
import sys
from math import ceil, log


reqHeaderToVar = {}
reqHeaderToVar['x265'] = {}
reqHeaderToVar['TAppEncoderStatic'] = {}

####### MANUAL ########
reqHeaderToVar['x265']['0_modNeighbourMV_LEFT'] = 'modNeighbours[0]'
reqHeaderToVar['x265']['0_modNeighbourMV_ABOVE'] = 'modNeighbours[1]'
reqHeaderToVar['x265']['0_modNeighbourMV_ABOVE_RIGHT'] = 'modNeighbours[2]'
reqHeaderToVar['x265']['0_modNeighbourMV_BELOW_LEFT'] = 'modNeighbours[3]'
reqHeaderToVar['x265']['0_modNeighbourMV_ABOVE_LEFT'] = 'modNeighbours[4]'
reqHeaderToVar['x265']['0_modNeighbourMV_COLLOCATED'] = 'modNeighbours[5]'
reqHeaderToVar['x265']['0_neighbourMV_LEFT_highest'] = 'neighboursMVHighest[0]'
reqHeaderToVar['x265']['0_neighbourMV_ABOVE_highest'] = 'neighboursMVHighest[1]'
reqHeaderToVar['x265']['0_neighbourMV_ABOVE_RIGHT_highest'] = 'neighboursMVHighest[2]'
reqHeaderToVar['x265']['0_neighbourMV_BELOW_LEFT_highest'] = 'neighboursMVHighest[3]'
reqHeaderToVar['x265']['0_neighbourMV_ABOVE_LEFT_highest'] = 'neighboursMVHighest[4]'
reqHeaderToVar['x265']['0_neighbourMV_COLLOCATED_highest'] = 'neighboursMVHighest[5]'
reqHeaderToVar['x265']['0_costAmvp0'] = 'costAmvp[0]'
reqHeaderToVar['x265']['0_costAmvp1'] = 'costAmvp[1]'

reqHeaderToVar['TAppEncoderStatic']['0_costAmvp0'] = 'costAmvp[0]'
reqHeaderToVar['TAppEncoderStatic']['0_costAmvp1'] = 'costAmvp[1]'
##################

dirFiles = '/home/brunno/Projetos/Mestrado/features_extraction/run_features_extraction/GENERAL_VIDEO_FEATURES/'
encoders = ['x265', 'TAppEncoder']
#encoders = ['TAppEncoderStatic']

nRows = 100000

#video = "BQMall"

for encoder in encoders:
	nameFileOut = "auto_bits_features_%s.txt" % (encoder)
	i = 0

	maxValues = {}

	for nameFile in os.listdir(dirFiles):
		if encoder in nameFile:
			print nameFile
			
			for chunk in pd.read_csv(dirFiles+nameFile, sep='\t', chunksize = nRows):
				if i == 0:
					for key in chunk.keys():
						maxValues[key] = 0

				i += 1

				for key in chunk.keys():
					maxValues[key] = max(maxValues[key], max(chunk[key]))

	fileOut = open(nameFileOut, "w")
	for key in maxValues.keys():
		print maxValues[key]
		ceilMaxValue = 0 if maxValues[key] == 0 else ceil(log(maxValues[key],2))
		bitsValue = ceilMaxValue
		if ((2**ceilMaxValue)-1) < maxValues[key]:
			bitsValue += 1
		
		if key in reqHeaderToVar[encoder].keys():
			print >> fileOut, "%s=%d" % (reqHeaderToVar[encoder][key], bitsValue)
		else:
			print >> fileOut, "%s=%d" % (key.split("_")[-1], bitsValue)

	fileOut.close()
