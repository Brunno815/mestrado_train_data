import numpy as np
import os

dirVideos = "../features_extraction/run_features_extraction/GENERAL_VIDEO_FEATURES"
#dirVideos = "VIDEO_FEATURES_EXAMPLE"

dirOut = "DISTRIBUTION_REPORTS"

qps = ["22","27","32","37"]
#qps = ["32","37"]

encoders = ["x265", "TAppEncoderStatic"]
#encoders = ["x265"]
stepBestFound = {}

for encoder in encoders:
	stepBestFound[encoder] = {}

N_ROWS = 10000

os.system("mkdir -p %s" % (dirOut))
f_report_videos = open("%s/report_videos.csv" % (dirOut), "w")
f_report_avgs = open("%s/report_avgs.csv" % (dirOut), "w")
f_report_qps = open("%s/report_qps.csv" % (dirOut), "w")

print >> f_report_videos, ",".join(["Video", "Encoder", "QP", "seek", "step_0", "step_1", "step_2", "step_3"])
print >> f_report_qps, ",".join(["Avg_qps", "Encoder", "step_0", "step_1", "step_2", "step_3"])
print >> f_report_avgs, ",".join(["Avg_videos", "Encoder", "step_0", "step_1", "step_2", "step_3"])


for nameFile in os.listdir(dirVideos):

	encoder = "x265" if ("x265" in nameFile) else "TAppEncoderStatic"

	i = 0

	videoName = nameFile.split("_")[0]
	qp = nameFile.split("_")[-2][2:]
	seek = nameFile.split("_")[-1].split(".")[0][4:]

	if videoName not in stepBestFound[encoder].keys():
		stepBestFound[encoder][videoName] = {}

	stepBestFound[encoder][videoName][qp] = np.zeros((4.0), dtype = int)

	while True:
		fileData = np.genfromtxt("/".join([dirVideos, nameFile]), skip_header = 1 + N_ROWS*i, delimiter= '\t', comments = "#", max_rows = N_ROWS)

		i += 1

		y = fileData[:,-1].astype(int)

		#print np.unique(y, return_counts = True)

		elems, counts = np.unique(y, return_counts = True)

		#print elems
		#print counts

		#print elems
		#print nameFile

		for idx, elem in enumerate(elems):
			stepBestFound[encoder][videoName][qp][elem] += counts[idx]

		if len(fileData) < N_ROWS:
			break

	print ",".join([videoName, encoder, qp, seek] + [str(x) for x in list(stepBestFound[encoder][videoName][qp])])
	print >> f_report_videos, ",".join([videoName, encoder, qp, seek] + [str(x) for x in list(stepBestFound[encoder][videoName][qp])])

avg_qps = {}
avg_seqs = {}
avg_indiv_videos = {}

for encoder in stepBestFound.keys():
	avg_indiv_videos[encoder] = {}
	avg_seqs[encoder] = {}
	avg_seqs[encoder] = np.zeros(4.0, dtype = float)

	for videoName in stepBestFound[encoder].keys():
		avg_indiv_videos[encoder] = np.zeros(4.0, dtype = float)

		for qp in qps:
			avg_indiv_videos[encoder] = np.add(avg_indiv_videos[encoder], stepBestFound[encoder][videoName][qp])
			avg_seqs[encoder] = np.add(avg_seqs[encoder], stepBestFound[encoder][videoName][qp])


		avg_indiv_videos[encoder] /= float(len(qps))

		print >> f_report_avgs, ",".join([videoName, encoder] + [str(x) for x in list(avg_indiv_videos[encoder])])

	avg_seqs[encoder] /= (float(len(qps))*len(stepBestFound[encoder]))

	print >> f_report_avgs, ",".join(["Avg", encoder] + [str(x) for x in list(avg_seqs[encoder])])
	print >> f_report_avgs, ",".join(["Distribution", encoder] + ["{0:.2f}%".format(x*100/sum(avg_seqs[encoder])) for x in list(avg_seqs[encoder])])


for encoder in stepBestFound.keys():
	avg_qps[encoder] = {}

	for qp in qps:
		avg_qps[encoder][qp] = np.zeros(4.0, dtype = float)

		for videoName in stepBestFound[encoder].keys():
			avg_qps[encoder][qp] = np.add(avg_qps[encoder][qp], stepBestFound[encoder][videoName][qp])

		avg_qps[encoder][qp] /= (len(stepBestFound[encoder].keys()))

		print >> f_report_qps, ",".join([qp, encoder] + [str(x) for x in list(avg_qps[encoder][qp])])
		print >> f_report_qps, ",".join(["Distribution", encoder] + ["{0:.2f}%".format(x*100/sum(avg_qps[encoder][qp])) for x in list(avg_qps[encoder][qp])])


f_report_videos.close()
f_report_qps.close()
f_report_avgs.close()
