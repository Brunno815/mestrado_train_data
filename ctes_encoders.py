encoders = ['x265', 'TAppEncoder']

nameDefaultClass = {}
nameDefaultClass['x265'] = 'comlab215'
nameDefaultClass['TAppEncoder'] = 'TComLab215'

nameDecisionClass = {}
nameDecisionClass['x265'] = 'decisiontree'
nameDecisionClass['TAppEncoder'] = 'TEncDecisionTree'

nameFunction = 'decisionTree'
nameEntity = 'decisionTree'

nameCommonDir = {}
nameCommonDir['x265'] = 'common'
nameCommonDir['TAppEncoder'] = 'TLibCommon'

dirPdf = {}
dirPdf['tree'] = 'PDF_trees'
dirPdf['forest'] = 'PDF_forests'

dirVhdl = 'VHDL'
dstDirVhdl = '~/Projetos/Mestrado/decision_tree/'

dstDirEncoder = {}
dstDirEncoder['x265'] = '~/Projetos/Mestrado/features_extraction/x265/source/encoder'
dstDirEncoder['TAppEncoder'] = '~/Projetos/Mestrado/features_extraction/HM-16.9/source/Lib/TLibEncoder'

dirCommon = {}
dirCommon['x265'] = '../features_extraction/x265/source/common'
dirCommon['TAppEncoder'] = '../features_extraction/HM-16.9/source/Lib/TLibCommon'

dirData = {}
dirData['x265'] = 'data_x265'
dirData['TAppEncoder'] = 'data_TAppEncoder'

trainFile = {}
trainFile['x265'] = 'train_data_x265.data'
trainFile['TAppEncoder'] = 'train_data_TAppEncoder.data'

testFile = {}
testFile['x265'] = 'test_data_x265.data'
testFile['TAppEncoder'] = 'test_data_TAppEncoder.data'

dirSizes = 'bits_features'

reqHeaderToVar = {}
reqHeaderToVar['x265'] = {}
reqHeaderToVar['TAppEncoder'] = {}

####### MANUAL ########
reqHeaderToVar['x265']['0_neighbourMV_LEFT_highest'] = 'neighboursMVHighest[0]'
reqHeaderToVar['x265']['0_neighbourMV_ABOVE_highest'] = 'neighboursMVHighest[1]'
reqHeaderToVar['x265']['0_neighbourMV_ABOVE_RIGHT_highest'] = 'neighboursMVHighest[2]'
reqHeaderToVar['x265']['0_neighbourMV_BELOW_LEFT_highest'] = 'neighboursMVHighest[3]'
reqHeaderToVar['x265']['0_neighbourMV_ABOVE_LEFT_highest'] = 'neighboursMVHighest[4]'
reqHeaderToVar['x265']['0_neighbourMV_COLLOCATED_highest'] = 'neighboursMVHighest[5]'
reqHeaderToVar['x265']['0_costAmvp0'] = 'costAmvp[0]'
reqHeaderToVar['x265']['0_costAmvp1'] = 'costAmvp[1]'

reqHeaderToVar['TAppEncoder']['0_costAmvp0'] = 'costAmvp[0]'
reqHeaderToVar['TAppEncoder']['0_costAmvp1'] = 'costAmvp[1]'
##################
