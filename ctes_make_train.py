N_ROWS = 30000

dirVideos = "../features_extraction/run_features_extraction/GENERAL_VIDEO_FEATURES"

#train sequences were chosen based on the distribution that were better in the step 0 of the IME (by the choice of the predictor), choosing the one with the slowest percentage and the one with the highest one (assuring that one video with 10 bits from class A would be chosen and that RaceHorses would not be repeated, even if it was the slowest with step 0)
trainSequences = ['Traffic', 'NebutaFestival', 'Cactus', 'BQTerrace', 'BQMall', 'RaceHorsesC', 'BasketballPass', 'BlowingBubbles']
testSequences = ['PeopleOnStreet', 'SteamLocomotiveTrain', 'BasketballDrive', 'Kimono1', 'ParkScene', 'BasketballDrill', 'PartyScene', 'RaceHorses', 'BQSquare']

encoders = ['x265', 'TAppEncoder']

mode = 'train'

sequences = trainSequences if mode == 'train' else testSequences

relabeling = {}
relabeling['x265'] = [0, 1]
relabeling['TAppEncoder'] = [0, 1]
#the previous ones always need to be executed, because the best candidate may have been found due to the decision of a previous stage,
#even though that's not the best stage. So, for 2, 1 and 2 must be executed.

dirOut = {}
dirOut['x265'] = 'data_x265'
dirOut['TAppEncoder'] = 'data_TAppEncoder'