import math
import sys
from os import system
from math import ceil
from itertools import combinations

#def print_file(fileHandler, codeLine):
#	print >> fileHandler, codeLine

def gen_comparator(fileHandler, input1, input2, output):
	print >> fileHandler, "process(%s, %s)" % (input1, input2)
	print >> fileHandler, "begin"
	print >> fileHandler, "\tif (%s < %s) then" % (input1, input2)
	print >> fileHandler, "\t\t%s <= '1';" % (output)
	print >> fileHandler, "\telsif (%s = %s) then" % (input1, input2)
	print >> fileHandler, "\t\t%s <= '1';" % (output)
	print >> fileHandler, "\telse"
	print >> fileHandler, "\t\t%s <= '0';" % (output)
	print >> fileHandler, "\tend if;"
	print >> fileHandler, "end process;"
	print >> fileHandler, "\n\n"


def gen_maj(fileHandler, inputs, output):
	
	nrInputsMaj = len(inputs)
	sizeTermMaj = int(ceil(nrInputsMaj/2.0))

	expr = ""
	nands = []

	for comb in combinations(inputs, sizeTermMaj):
		nands.append("not(%s)" % (" and ".join(comb)))

	expr = "%s <= not(%s);" % (output, " and ".join(nands))

	print >> fileHandler, "\n"
	print >> fileHandler, "%s" % (expr)
	print >> fileHandler, "\n\n"


def gen_libraries(fileHandler, customLibraries = []):
	print >> fileHandler, "library IEEE;"
	print >> fileHandler, "use IEEE.STD_LOGIC_1164.ALL;"
	print >> fileHandler, "use IEEE.STD_LOGIC_UNSIGNED.ALL;"

	for customLibrary in customLibraries:
		print >> fileHandler, "use %s;" % (customLibrary)

	print >> fileHandler, "\n\n"


def gen_entity(fileHandler, nameEntity, inputs, outputs):
	print >> fileHandler, "entity %s is" % (nameEntity)
	print >> fileHandler, "\tPort("
	
	for _input in inputs:
		if _input["size"] > 1:
			print >> fileHandler, "\t\t%s: in %s(%d downto 0);" % (_input["name"], _input["type"], _input["size"] - 1)
		else:
			print >> fileHandler, "\t\t%s: in %s;" % (_input["name"], _input["type"])

	for idx, output in enumerate(outputs):
		if output["size"] > 1:
			print >> fileHandler, "\t\t%s: out %s(%d downto 0)%s" % (output["name"], output["type"], output["size"] - 1, ";" if idx != (len(outputs) - 1) else "")
		else:
			print >> fileHandler, "\t\t%s: out %s%s" % (output["name"], output["type"], ";" if idx != (len(outputs) - 1) else "")

	print >> fileHandler, "\t);"
	print >> fileHandler, "end %s;\n\n" % (nameEntity)


def gen_signals(fileHandler, signals):
	for signal in signals:
		if signal["size"] > 1:
			print >> fileHandler, "signal %s: %s(%d downto 0)%s;" % (signal["name"], signal["type"], signal["size"]-1, " := \"%s\"" % (signal["const_value"]) if "const_value" in signal.keys() else "")
		else:
			print >> fileHandler, "signal %s: %s%s;" % (signal["name"], signal["type"], " := \'%s\'" % (signal["const_value"]) if "const_value" in signal.keys() else "")

	print >> fileHandler, "\n\n"


def gen_logic(fileHandler, attrs):
	for attr in attrs:
		print >> fileHandler, "%s <= %s %s %s;\n" % (attr[0], attr[1], attr[2], attr[3])


def gen_ands_ors(fileHandler, exprs):
	for expr in exprs:
		print >> fileHandler, "%s <= %s;\n" % (expr["out"], (" %s " % (expr["op"])).join(expr["ops"]))
		

def gen_architecture(fileHandler, nameEntity, signals, comparators, exprs, maj):
	
	print >> fileHandler, "architecture Behavioral of %s is\n" % (nameEntity)

	gen_signals(fileHandler, signals)

	print >> fileHandler, "begin\n"

	for comparator in comparators:
		gen_comparator(fileHandler, comparator["in1"], comparator["in2"], comparator["out"])

	gen_ands_ors(fileHandler, exprs)

	if maj != []:
		gen_maj(fileHandler, maj['inputs'], maj['output'])

	print >> fileHandler, "end Behavioral;"






'''
with open("decision_tree.vhd", "w") as fHandler:
	
	nameEntity = "decision_tree"

	gen_libraries(fHandler)
	gen_entity(fHandler, nameEntity, inputs, outputs)
	gen_architecture(fHandler, nameEntity, signals, codes)

'''