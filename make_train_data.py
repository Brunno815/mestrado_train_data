import numpy as np 
import pandas as pd 
import os  
import matplotlib.pyplot as plt

os.environ["PATH"] += os.pathsep + ''

from imblearn.under_sampling import RandomUnderSampler
from sklearn import preprocessing

from ctes_make_train import *


def get_sequence_name(source):
	return source.split('_')[0]


def convert_str_to_num(d):

	le = preprocessing.LabelEncoder()

	for col in range(d.shape[1]):
		try:
			d[:,col] = d[:,col].astype(float)
		except:
			d[:,col] = le.fit_transform(d[:,col])

	return d



##########################################################################

for encoder in encoders:

	allData = np.empty(0)

	##### CREATE THE OUTPUT FOLDER AND OPEN THE FILE #####
	os.system('mkdir -p %s' % (dirOut[encoder]))
	outFile = open('%s/%s_data_%s.data' % (dirOut[encoder], mode, encoder),'w')
	####################################

	##### GET EVERY FILE NAME FROM THE dirVideos DIRECTORY (WHICH CONTAINS THE FEATURES) #####
	sources = [_ for _ in os.listdir(dirVideos) if  '.csv' in _ and encoder in  _]
	##########################################################################################

	##### GETS EVERY FEATURE FROM ONE FILE (WHICH WILL BE THE SAME FEATURES FOR ALL OF THEM, FOR THE SAME ENCODER) #####
	features = open("%s/%s" % (dirVideos, sources[0]),'r').readline().strip('\n').strip('#').split('\t')
	####################################################################################################################

	##### GETS THE INDEX OF THE LAST FEATURES OF EACH STEP AND OF THE y VECTOR #####
	yIdx = features.index('stepBestFound')
	#idxLastFeatures = [max([features.index(_) for _ in features if x in _]) for x in ["0_", "1_", "2_"]]
	lastZeroFeat = max([features.index(_) for _ in features if '0_' in _])
	################################################################################

	##### GETS ALL THE FEATURES OF THE SPECIFIC STEP AND PRINT EVERY FEATURE TO OUTFILE #####
	features = features[:lastZeroFeat]
	print  >> outFile, ','.join(features+['stepBestFound'])
	#########################################################################################

	##### ITERATE THROUGH EVERY VIDEO SOURCE #####
	for source in sources:
		seq = get_sequence_name(source)
		if seq not in sequences:
			continue

		numSamples = 0
		i = 0
		print source

		##### READ N_ROWS FOR EACH ITERATION UNTIL IT REACHES THE NUMBER OF REQUIRED ROWS (WHICH IS N_ROWS AS WELL, BUT THE READ DATA IS SUBSAMPLED) FOR THE FINAL FILE FOR EACH SOURCE #####
		while numSamples < N_ROWS:
			try:
				tmpData = np.genfromtxt("%s/%s" % (dirVideos, source), skip_header = 1 + N_ROWS*i, delimiter= '\t', max_rows = N_ROWS)
			except:
				if numSamples<N_ROWS:
					print 'Could not fulfill n_rows for file %s' % source
					break
			i += 1

			##### SEPARATE DATA FROM FILE BETWEEN X (features) AND y (OUTPUT VECTOR) #####
			y = tmpData[:,yIdx].astype(int)
			X = tmpData[:,:lastZeroFeat]
			##############################################################################

			############### RELABEL OUTPUTS HIGHER THAN 1 TO BE 1 (EXECUTE IME OR NOT) ###############
			##### ALSO UNDERSAMPLES THE XR AND YR VECTOR TO HAVE THE SAME NUMBER OF OUTPUTS IN Y #####
			y[y > 1] = 1
			print np.unique(y)
			rus = RandomUnderSampler()
			Xr,yr = rus.fit_sample(X,y)

			numSamples += yr.shape[0]
			##########################################################################################
			##########################################################################################

			##### CONCATENATE THE DATA FROM XR AND yR IN allData #####
			if allData.shape[0] == 0:
				allData = np.hstack((Xr,yr.reshape(-1,1)))
			else:
				tmpData = np.hstack((Xr,yr.reshape(-1,1)))
				allData = np.vstack((allData, tmpData))
			##########################################################
		#####################################################################################################################################################################################

		##### DETERMINE DATA WHOSE OUTPUT VECTOR (LAST COLUMN) IS ZERO AND ONE #####
		allData0 = allData[allData[:,-1] == 0,:]
		allData1 = allData[allData[:,-1] == 1,:]
		############################################################################

		##### GET THE NUMBER OF SAMPLES #####
		maxSamples = min(allData0.shape[0],allData1.shape[0])
		maxSamples = min(N_ROWS/2,maxSamples)
		#####################################

		##### SAVE THE allData0 AND allData1 STRUCTURES TO THE allData STACK AND SAVE IT TO THE FILE #####
		allData = np.vstack((allData0[:maxSamples,:],allData1[:maxSamples,:]))
		np.savetxt(outFile, allData[:,:], delimiter= ',', fmt ='%g')
		##################################################################################################

	##############################################
	outFile.close()

exit()



#	labelFeatures = [0]*(len(relabeling[encoder])-1)
#	for labels in relabeling[encoder][1:]:
#		labelFeatures[labels-1] = features[:idxLastFeatures[labels-1]]
#
#	featuresByStep = []
#	for idx, _labelFeatures in enumerate(labelFeatures):
#		allData = np.empty(0)
#
#		outFile = open('%s/%s_data_%d.data' % (dirOut[encoder], mode, idx), 'w')
#
#		print >> outFile, ','.join(_labelFeatures + ['stepBestFound'])
#
#		for source in sources:
#			seq = get_sequence_name(source)
#			if seq not in sequences:
#				continue
#
#			numSamples = 0
#			i = 0
#			print source
#			while numSamples < N_ROWS:
#				try:
#					tmpData = np.genfromtxt("%s/%s" % (dirVideos, source), skip_header = 1 + N_ROWS*i, delimiter='\t', max_rows = N_ROWS)
#				except:
#					if numSamples < N_ROWS:
#						print 'Could not fulfill n_rows for file %s' % (source)
#						break
#
#				i += 1
#
#				y = tmpData[:,yIdx].astype(int)
#				X = tmpData[:,:idxLastFeatures[]] #ARRUMAR

#				#X = np.nan_to_num(X)
#				refineData = np.hstack((X,y.reshape(-1,1)))
#				refineData = refineData[np.logical_or(refineData[:,-1] == SOMETHING, refineData[:,-1] == SOMETHING)]
#
#				print refineData
#				print len(refineData)
#				#y[y > 1] = 1
#
#
#
#				print np.unique(y)
#				rus = RandomUnderSampler()
#				Xr, yr = rus.fit_sample(X, y)
#
#				numSamples += yr.shape[0]
#
#				if allData.shape[0] == 0:
#					allData = np.hstack((Xr,yr.reshape(-1,1)))
#				else:
#					tmpData = np.hstack((Xr,yr.reshape(-1,1)))
#					allData = np.vstack((allData, tmpData))
#
#
#			dataStep = [0]*4
#			for i in range(4):
#				dataStep[i] = allData[allData[:,-1] == i,:]
#
#			dataMaxSamples = min(min([dataStep[i].shape[0] for i in range(4)]),N_ROWS/nValidFeatures)
#			tupAllData = ()
#			for i in range(4):
#				tupAllData += dataStep[i][:dataMaxSamples,:]
#			allData = np.vstack(tuple([dataStep[i][:dataMaxSamples,:] for i in range(4)]))
#			np.savetxt(outFile, allData[:,:], delimiter= ',', fmt ='%g')
#
#		outFile.close()
