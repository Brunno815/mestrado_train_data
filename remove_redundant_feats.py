import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import os
import seaborn as sns
from sklearn.feature_selection import VarianceThreshold

path_hm = './data_TAppEncoder/train_data_TAppEncoder.data'

data = pd.read_csv(path_hm)

data = data.loc[:, [len(data[_].unique()) != 1 for _ in data.columns]]

fig, ax = plt.subplots(figsize=(50,50))
data.hist(ax = ax)
plt.show()

fig, ax = plt.subplots(figsize=(20,20))

sns.heatmap(data.corr().abs())
plt.show()

X = data.loc[:,data.columns != 'stepBestFound']
y = data['stepBestFound']

th = 0.9
rem = []

for idx, f in enumerate(X.columns):
    for i in X.columns[idx+1:]:
        corr = abs(np.corrcoef(X[f],X[i]))[0][1]
        corr_f = abs(np.corrcoef(X[f],y))[0][1]
        corr_i = abs(np.corrcoef(X[i],y))[0][1]

        if corr > th:
            if corr_f > corr_i:
                rem = rem if i in rem else rem + [i]
            else:
                rem = rem if f in rem else rem + [f]
    

X = X.drop(rem, axis = 1)

print X.shape

fig, ax = plt.subplots(figsize=(20,20))

sns.heatmap(X.corr().abs())
plt.show()

fig, ax = plt.subplots(figsize=(50,50))
X.hist(ax = ax)
plt.show()

fig, ax = plt.subplots(figsize=(20,10))

plt.bar(range(X.shape[1]),X.corrwith(y).abs())
plt.xticks(range(X.shape[1]),X.columns, rotation=90)
plt.show()

