import os
from os import system
import glob
from Bjontegaard import bdrate

testSequences = [['PeopleOnStreet_2560x1600_30_crop', '2560', '1600', '30', '30', 'PeopleOnStreet', "PeopleOnStreet"], ["SteamLocomotiveTrain_2560x1600_60_10bit_crop","2560","1600","60","60","SteamLocomotiveTrain_10bit", "SteamLocomotiveTrain"], ["BasketballDrive_1920x1080_50","1920","1080","50","50","BasketballDrive", "BasketballDrive"], ["Kimono1_1920x1080_24","1920","1080","24","24","Kimono", "Kimono"], ["ParkScene_1920x1080_24","1920","1080","24","24","ParkScene","ParkScene"], ["BasketballDrill_832x480_50","832","480","50","50","BasketballDrill","BasketballDrill"], ["PartyScene_832x480_50","832","480","50","50","PartyScene","PartyScene"], ["RaceHorses_416x240_30","416","240","30","30","RaceHorses","RaceHorses"], ["BQSquare_416x240_60","416","240","60","60","BQSquare","BQSquare"]]

qps = ['22','27','32','37']
encoders = ['x265', 'TAppEncoderStatic']
header = '\t'.join(['Video', 'Encoder', 'Seek', 'Frames', 'BDRate-Y', 'BDRate-U', 'BDRate-V', 'BDRate-YUV'])

dirQuality = 'QUALITY_TESTSEQUENCES'

with open('quality_results.csv', 'w') as f_quality:
	print >> f_quality, header

	param_ref = []
	param = []

	for encoder in encoders:
		dirQualityEncoder = dirQuality.split("_")[0] + "_" + encoder

		for testSequence in testSequences:

			if(len(glob.glob("%s/%s/*quality_%s*" % (dirQuality, dirQualityEncoder, testSequence[6]))) == 0):
				continue

			param_ref = []
			param = []

			for qp in qps:
				nameFileDef = glob.glob("%s/%s/def_quality_%s_qp_%s_seek_*" % (dirQuality, dirQualityEncoder, testSequence[6], qp))[0]
				with open('%s' % (nameFileDef), 'r') as f_in:
					if encoder == 'x265':
						line = f_in.readlines()[1].split(", ")
						param_ref.insert(len(param_ref), [float(line[4])] + [float(x) for x in line[5:9]])
					else:
						lines = f_in.readlines()
						for line in lines:
							if "SUMMARY" in line:
								param_ref.insert(len(param_ref), [float(x) for x in lines[lines.index(line)+2].split()[2:7]])


				nameFileTest = glob.glob("%s/%s/quality_%s_qp_%s_seek_*" % (dirQuality, dirQualityEncoder, testSequence[6], qp))[0]
				with open('%s' % (nameFileTest), 'r') as f_in:
					if encoder == 'x265':
						line = f_in.readlines()[1].split(", ")
						param.insert(len(param), [float(line[4])] + [float(x) for x in line[5:9]])
					else:
						lines = f_in.readlines()
						for line in lines:
							if "SUMMARY" in line:
								param.insert(len(param), [float(x) for x in lines[lines.index(line)+2].split()[2:7]])

			print param_ref
			print param

			print >> f_quality, "\t".join([testSequence[6], encoder, nameFileTest.split("_")[nameFileTest.split("_").index('seek')+1], nameFileTest.split("_")[-1].split(".")[0]] + [str(x) for x in bdrate(param_ref, param)])
