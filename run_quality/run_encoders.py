import os
from os import system
from random import randint
from itertools import product

encoders = ['x265', 'TAppEncoderStatic']
testSequences = ['PeopleOnStreet', 'SteamLocomotiveTrain', 'BasketballDrive', 'Kimono1', 'ParkScene', 'BasketballDrill', 'PartyScene', 'RaceHorses', 'BQSquare']

# [name_yuv, width, height, fps, frames, name_config, name]
testSequences = [['PeopleOnStreet_2560x1600_30_crop', '2560', '1600', '30', '30', 'PeopleOnStreet', "PeopleOnStreet"], ["SteamLocomotiveTrain_2560x1600_60_10bit_crop","2560","1600","60","60","SteamLocomotiveTrain_10bit", "SteamLocomotiveTrain"], ["BasketballDrive_1920x1080_50","1920","1080","50","50","BasketballDrive", "BasketballDrive"], ["Kimono1_1920x1080_24","1920","1080","24","24","Kimono", "Kimono"], ["ParkScene_1920x1080_24","1920","1080","24","24","ParkScene","ParkScene"], ["BasketballDrill_832x480_50","832","480","50","50","BasketballDrill","BasketballDrill"], ["PartyScene_832x480_50","832","480","50","50","PartyScene","PartyScene"], ["RaceHorses_416x240_30","416","240","30","30","RaceHorses","RaceHorses"], ["BQSquare_416x240_60","416","240","60","60","BQSquare","BQSquare"]]

trainSequences = [["BQTerrace_1920x1080_60","1920","1080","60","60","BQTerrace","BQTerrace"],["Cactus_1920x1080_50","1920","1080","50","50","Cactus","Cactus"],["BasketballPass_416x240_50","416","240","50","50","BasketballPass","BasketballPass"],["RaceHorses_832x480_30","832","480","30","30","RaceHorsesC","RaceHorsesC"],["Traffic_2560x1600_30_crop","2560","1600","30","30","Traffic","Traffic"],["NebutaFestival_2560x1600_60_10bit_crop","2560","1600","60","60","NebutaFestival_10bit","NebutaFestival"],["BQMall_832x480_60","832","480","60","60","BQMall","BQMall"],["BlowingBubbles_416x240_50","416","240","50","50","BlowingBubbles","BlowingBubbles"]]

qps = ['22','27','32','37']

dirEncoders = {}
dirEncoders['x265'] = '~/Projetos/Mestrado/test_data/x265/build/linux'
dirEncoders['TAppEncoderStatic'] = '~/Projetos/Mestrado/test_data/HM-16.9/bin'

cfgDir = {}
cfgDir['x265'] = ''
cfgDir['TAppEncoderStatic'] = '~/Projetos/Mestrado/test_data/HM-16.9/cfg'

params_tree = ['0','1']


defFileAppend = 'def_'

dirQuality = "QUALITY_TESTSEQUENCES"

system('mkdir -p %s' % (dirQuality))

for encoder in encoders:
	dirQualityEncoder = '%s/%s' % (dirQuality, dirQuality.split("_")[0] + "_" + encoder)

	system('mkdir -p %s' % (dirQualityEncoder))

	for testSequence in testSequences:
		seek_value = randint(0, 30)
		for param_tree in params_tree:
			for qp in qps:
				if encoder == 'x265':
					cmd = '%s/%s --input ~/origCfP/%s.yuv --input-res %sx%s --fps %s -f %s --seek %s -o str.bin --pools none --frame-threads 1 --qp %s --no-wpp --psnr --csv %s/%squality_%s_qp_%s_seek_%s_f_%s.csv --exec-decision %s' % (dirEncoders[encoder], encoder, testSequence[0], testSequence[1], testSequence[2], testSequence[3], testSequence[4], seek_value, qp, dirQualityEncoder, defFileAppend if param_tree == '0' else "", testSequence[6], qp, seek_value, testSequence[4], param_tree)
				else:
					cmd = '%s/%s -c %s/encoder_lowdelay_main.cfg -c %s/per-sequence/%s.cfg -i ~/origCfP/%s.yuv -q %s -f %s -fs %s -exd %s > %s/%squality_%s_qp_%s_seek_%s_f_%s.csv' % (dirEncoders[encoder], encoder, cfgDir[encoder], cfgDir[encoder], testSequence[5], testSequence[0], qp, testSequence[4], seek_value, param_tree, dirQualityEncoder, defFileAppend if param_tree == '0' else "", testSequence[6], qp, seek_value, testSequence[4])

				system(cmd)

				system('rm str.bin')