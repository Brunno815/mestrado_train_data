from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sys import argv
import numpy as np
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.model_selection import cross_val_score
import graphviz 
import os
from sklearn.tree import _tree
import copy
from copy import deepcopy
import gen_decision_tree as vhdl
import sys
import boolean
from ctes_encoders import *
import pandas as pd
from itertools import product


def remove_feats(feats, list_remove, trainData, testData):
	for rem_feat in list_remove:
		if rem_feat in feats:
			feat_idx = feats.index(rem_feat)
			trainData = np.delete(trainData, [feat_idx], axis = 1)
			testData = np.delete(testData, [feat_idx], axis = 1)
			feats.remove(rem_feat)

	return feats, trainData, testData


def print_corrs(feats, X, y):
	for col in range(X.shape[1]):
		corr = np.corrcoef(X[:,col], y)
		if corr[0][1] == np.nan:
			corr[0][1] = 0
		print col,feats[col], corr[0][1]


def get_all_comparisons_ors(tree, featureNames):
	tree_ = tree.tree_
	featureName = [featureNames[i][1] if i != _tree.TREE_UNDEFINED else "undefined!" for i in tree_.feature]
	featureSize = [featureNames[i][2] if i != _tree.TREE_UNDEFINED else "undefined!" for i in tree_.feature]

	comparisons = []

	ors = []

	def recurse(node, depth, expression):
		indent = "\t" * depth
		if tree_.feature[node] != _tree.TREE_UNDEFINED:
			name = featureName[node]
			threshold = int(tree_.threshold[node])
			_type = int(featureSize[node])

			if [name, threshold] not in comparisons:
				comparisons.append([name, threshold, _type])

			recurse(tree_.children_left[node], depth + 1, deepcopy(expression + [[name, threshold, _type, 'true']]))

			recurse(tree_.children_right[node], depth + 1, deepcopy(expression + [[name, threshold, _type, 'false']]))
		else:
			if str(np.argmax(tree_.value[node])) == "1":
				ors.append(deepcopy(expression))

	recurse(0, 1, [])

	return comparisons, ors


def get_simplified_expression(tree, featureNames, c, comparisons, ors):

	algebra = boolean.BooleanAlgebra()

	##### PARSE THE CONDITIONS INTO 'c%d' VALUES TO SIMPLIFY THEM USING BOOLEAN LIBRARY #####
	strExpr = ""
	for idxOr, elemOr in enumerate(ors):

		for idxAnd, elemAnd in enumerate(elemOr):

			idxElem = comparisons.index(elemAnd[:-1])
			notSymbol = "~" if elemAnd[3] == 'false' else ""
			condition = "%sc%s" % (notSymbol, idxElem)

			if idxOr == (len(ors) - 1):
				if idxAnd == 0 and idxAnd != (len(elemOr)-1):
					strExpr += '(%s&' % (condition)
				elif idxAnd == (len(elemOr)-1):
					strExpr += '%s)' % (condition)
				else:
					strExpr += '%s&' % (condition)
			else:
				if idxAnd == 0 and idxAnd != (len(elemOr)-1):
					strExpr += '(%s&' % (condition)
				elif idxAnd == (len(elemOr)-1):
					strExpr += '%s)|' % (condition)
				else:
					strExpr += '%s&' % (condition)
	#########################################################################################

	simplExpr = algebra.parse(strExpr).simplify()

	##### ISUSEDINEXPR IS NEEDED TO CHECK LATER WHICH OF THE COMPARISONS ARE NOT USED IN THE EXPRESSION (THEREFORE, ONLY LEAD TO '0' LEAVES) #####
	isUsedInExpr = [1]*len(comparisons)

	for idx, comparison in enumerate(comparisons):
		if "c%s" % (idx) not in str(simplExpr):
			isUsedInExpr[idx] = 0
	##############################################################################################################################################

	return simplExpr, isUsedInExpr


def tree_to_vhdl(clf, classifier, featureNames, fHandler, numTrees):

	##### CREATE OUTPUTS VECTOR WITH ONE OUTPUT (ONE BIT OF DECISION TREE, FIXED) #####
	outputs = []
	output = {}
	output['name'] = 'decision'
	output['type'] = 'STD_LOGIC'
	output['size'] = 1
	outputs.append(output)
	###################################################################################

	##### GET ALL UNIQUE COMPARISONS IN EACH TREE (as the tree may compare the same thing twice in different places) AS WELL AS ALL PATHS (ORS) THAT LEAD TO 1 #####
	#comparisons = [['var','value', 'size'],[],...]
	comparisons = []
	ors = []
	comparison = None
	_or = None

	for i in range(numTrees):
		if classifier == 'tree':
			comparison, _or = get_all_comparisons_ors(clf, featureNames)
		elif classifier == 'forest':
			comparison, _or = get_all_comparisons_ors(clf.estimators_[i], featureNames)
		comparisons.append(comparison)
		ors.append(_or)

	###############################################################################################################################################################

	################ DECLARE ONE BOOLEAN VARIABLE FOR EACH COMPARISON, TO CALL THE SIMPLIFIED_EXPRESSION ################
	##### GET_SIMPLIFIED_EXPRESSION GETS ALL THE PATHS WHOSE LEAVES ARE '1' AND SIMPLIFIES THE RESULTING EXPRESSION #####
	algebra = boolean.BooleanAlgebra()

	c = [0]*numTrees
	expressions = []
	isUsedInExprs = []
	strExprs = []

	for i in range(numTrees):
		c[i] = [0]*len(comparisons[i])
		for idx, comparison in enumerate(comparisons[i]):
			c[i][idx] = algebra.symbols("c%d" % (idx))
		
		expression = []
		isUsedInExpr = []

		if classifier == 'tree':
			expression, isUsedInExpr = get_simplified_expression(clf, featureNames, c[i], comparisons[i], ors[i])
		elif classifier == 'forest':
			expression, isUsedInExpr = get_simplified_expression(clf.estimators_[i], featureNames, c[i], comparisons[i], ors[i])

		expressions.append(expression)
		isUsedInExprs.append(isUsedInExpr)
		strExprs.append(str(expression))
	#####################################################################################################################

	##### CREATE INPUTS VECTOR WHOSE INPUTS ARE THE FEATURES USED IN THE TREE #####
	inputs = []
	existingInputs = []

	for i in range(numTrees):
		for idx, comparison in enumerate(comparisons[i]):
			if comparison[0].replace("[","_").replace("]","") not in existingInputs and isUsedInExprs[i][idx] == 1:
				existingInputs.append(comparison[0])
				_input = {}
				_input['name'] = comparison[0].replace("[","_").replace("]","")
				_input['type'] = 'STD_LOGIC' if comparison[2] == 1 else 'STD_LOGIC_VECTOR'
				_input['size'] = comparison[2]
				inputs.append(_input)

	###############################################################################

	##### WRITE THE LIBRARY NAMES AND THE ENTITY, WITH INPUTS AND OUTPUTS #####
	#print inputs
	vhdl.gen_libraries(fHandler)
	vhdl.gen_entity(fHandler, nameEntity, inputs, outputs)
	###########################################################################

	# TODO: Make separate function for this
	##### GET COMPARATORS AND SIGNALS #####
	signals = []
	comparators = []

	for i in range(numTrees):
		for idx, comparison in enumerate(comparisons[i]):
			filteredComparison = "_".join([comparison[0].replace("[","_").replace("]",""), str(comparison[1]).replace(".","_")])

			if isUsedInExprs[i][idx] == 1:
				signal = {}
				signal["name"] = "comp_%s" % (filteredComparison)
				signal["size"] = 1
				signal["type"] = "STD_LOGIC"
				signals.append(deepcopy(signal))

				if "const_%s_%s" % (str(comparison[1]).replace(".","_"), str(comparison[2])) not in [existingSignal["name"].replace(".","_") for existingSignal in signals]:
					signal = {}
					signal["name"] = "const_%s_%s" % (str(comparison[1]).replace(".","_"), str(comparison[2]))

					for _input in inputs:
						if _input['name'] == comparison[0].replace("[","_").replace("]",""):
							signal['type'] = _input["type"]
							signal["size"] = _input["size"]
							signal["const_value"] = "%s" % (('{0:0%sb}' % (signal["size"])).format(int(comparison[1])).replace("-",""))
							signals.append(deepcopy(signal))

				comparator = {}
				comparator["in1"] = comparison[0].replace("[","_").replace("]","")
				comparator["in2"] = "const_%s_%s" % (str(comparison[1]).replace(".","_"), str(comparison[2]))
				comparator["out"] = "comp_%s" % (filteredComparison)
				comparators.append(deepcopy(comparator))

	###################################################

	exprs = []

	##### GET 'AND' AND 'OR' SIGNALS (EXPRESSIONS) #####

	lastIdx = 0
	for i in range(numTrees):
		for idx, or_ in enumerate(strExprs[i].split("|")):
			signal = {}
			expr = {}
			expr["out"] = "and_%d" % (idx + lastIdx)
			expr["op"] = "and"
			expr["ops"] = []

			for idComp, comparison in enumerate(comparisons[i]):
				filteredComparison = "_".join([comparison[0].replace("[","_").replace("]",""), str(comparison[1]).replace(".","_")])
				if "c%d" % (idComp) in or_.strip("(").strip(")").split("&"):
					expr["ops"].append(deepcopy("comp_%s" % (filteredComparison)))
				elif "~c%d" % (idComp) in or_.strip("(").strip(")").split("&"):
					expr["ops"].append(deepcopy("not(comp_%s)" % (filteredComparison)))

			exprs.append(deepcopy(expr))

			signal["name"] = "and_%d" % (idx + lastIdx)
			signal["size"] = 1
			signal["type"] = "STD_LOGIC"
			signals.append(deepcopy(signal))

		signal = {}
		signal["name"] = "or_%d" % (i)
		signal["size"] = 1
		signal["type"] = "STD_LOGIC"
		signals.append(deepcopy(signal))

		expr["out"] = "or_%d" % (i)
		expr["op"] = "or"
		expr["ops"] = []

		expr["ops"] = ["and_%d" % (x + lastIdx) for x in range(len(strExprs[i].split("|")))]

		exprs.append(deepcopy(expr))

		lastIdx = lastIdx + idx + 1

	####################################


	##### GEN MAJORITARY GATE FOR ALL THE ORS #####

	maj = {}
	maj['inputs'] = []
	maj['output'] = 'decision'
	for i in range(numTrees):
		maj['inputs'].append('or_%d' % (i))

	###############################################

	if numTrees == 1:
		maj = []

	vhdl.gen_architecture(fHandler, nameEntity, signals, comparators, exprs, maj)


def tree_to_code(tree, featureNames, fHandler, nameFunction, nameClass):    
	tree_ = tree.tree_

	featureName = [featureNames[i][1] if i != _tree.TREE_UNDEFINED else "undefined!" for i in tree_.feature]

	print >> fHandler, 'bool %s(){' % (nameFunction)

	def recurse(node, depth):
		indent = "\t" * depth
		if tree_.feature[node] != _tree.TREE_UNDEFINED:
			name = featureName[node]
			threshold = int(tree_.threshold[node])

			print >> fHandler, "%sif(%s::%s <= %s)" % (indent, nameClass, name, threshold)

			recurse(tree_.children_left[node], depth + 1)

			print >> fHandler, "%selse //if %s::%s > %s" % (indent, nameClass, name, threshold)

			recurse(tree_.children_right[node], depth + 1)
		else:
			print >> fHandler, "%sreturn %s;" % (indent, np.argmax(tree_.value[node]))

	recurse(0, 1)

def code_maj(fHandler, nameFunction, numTrees):
	print >> fHandler, "bool %s(){" % (nameFunction)
	print >> fHandler, "\tint votes = 0;"
	for i in range(numTrees):
		print >> fHandler, "\tvotes += %s_%d();" % (nameFunction, i)
	print >> fHandler, "\tif(votes >= ceil(%s/2))" % (numTrees)
	print >> fHandler, "\t\treturn 1;"
	print >> fHandler, "\telse"
	print >> fHandler, "\t\treturn 0;"
	print >> fHandler, "}"


def gen_class(clf, classifier, featsNamed, fileH, fileCpp, nameCommonDir, nameDefaultClass, nameFunction, nameDecisionClass, numTrees = 5):

	print >> fileH, "#include \"../%s/%s.h\"" % (nameCommonDir, nameDefaultClass)
	print >> fileH, "\n\n"
	print >> fileH, "#ifndef DECISIONTREE_H"
	print >> fileH, "#define DECISIONTREE_H\n\n"
	print >> fileH, "bool %s();" % (nameFunction)

	if classifier == 'forest':
		for i in range(numTrees):
			print >> fileH, "bool %s_%d();" % (nameFunction, i)

	print >> fileH, "\n#endif"

	print >> fileCpp, "#include \"%s.h\"\n\n" % (nameDecisionClass)
	if classifier == 'tree':
		tree_to_code(clf, featsNamed, fileCpp, nameFunction, nameDefaultClass)
	elif classifier == 'forest':
		code_maj(fileCpp, nameFunction, numTrees)
		for i in range(numTrees):
			tree_to_code(clf.estimators_[i], featsNamed, fileCpp, "%s_%d" % (nameFunction, i), nameDefaultClass)
			print >> fileCpp, "\n"
			
	print >> fileCpp, "}"


def fill_feats_named(feats, encoder, nameDefaultClass, reqHeaderToVar):

	featsNamed = []

	for feat in feats:
		with open('%s/auto_%s_%s.txt' % (dirSizes, dirSizes, encoder), 'r') as fFeatures:

			origFeat = reqHeaderToVar[feat] if feat in reqHeaderToVar.keys() else feat.split("_")[1]

			for line in fFeatures.readlines():
				splitLine = [x for x in line.strip("\n").split("=") if x]
				if origFeat == splitLine[0]:
					if feat in reqHeaderToVar.keys():
						featsNamed.append([feat, reqHeaderToVar[feat], int(splitLine[1])])
					else:
						featsNamed.append([feat, splitLine[0], int(splitLine[1])])
					continue
	return featsNamed


def plot_PDF(clf, encoder, classifier = 'tree', path = 'tree', feats = None, labels = None, numTrees=5):
	os.system('mkdir -p %s/PDF_%s' % (path, encoder))
	if classifier == 'forest':
		for i in range(numTrees):
			dot_data = export_graphviz(clf.estimators_[i], out_file=None, feature_names=feats, class_names= labels, filled=True, rounded=True, special_characters=True)
			graph = graphviz.Source(dot_data)
			graph.render("%s/PDF_%s/%s_%s_%d" % (path, encoder, classifier, encoder, i))
	else:
		dot_data = export_graphviz(clf, out_file=None, feature_names=feats, class_names= labels, filled=True, rounded=True, special_characters=True)
		graph = graphviz.Source(dot_data)
		graph.render("%s/PDF_%s/%s_%s" % (path, encoder, classifier, encoder))





def train_data(classifier, numTrees, maxDepth):

	featuresToRemove = ['0_frameCount', '0_mightNotSplit', '0_interPredCount']
	featsNamed = {}

	os.system('rm -rf %s' % (dirVhdl))
	os.system('rm -rf %s' % (dirPdf[classifier]))
	os.system('mkdir -p %s' % (dirVhdl))
	os.system('mkdir -p %s' % (dirPdf[classifier]))

	hitValues = []

	for encoder in encoders:

		##### READ FEATURE STRINGS AND TRAIN/TEST DATA #####
		print "%s: Reading train and test data..." % (encoder)
		feats = open("%s/%s" % (dirData[encoder], trainFile[encoder]), 'r').readline().strip('\n').split(',')[:-1]
		trainData = np.genfromtxt("%s/%s" % (dirData[encoder], trainFile[encoder]), delimiter = ',', skip_header=1)

		testData = np.genfromtxt("%s/%s" % (dirData[encoder], testFile[encoder]), delimiter = ',', skip_header=1)
		####################################################

		##### OPEN OUTPUT FILES #####
		fileOutCpp = open("%s.cpp" % (nameDecisionClass[encoder]), 'w')
		fileOutH = open("%s.h" % (nameDecisionClass[encoder]), 'w')
		fileOutVhdl = open("%s/%s.vhd" % (dirVhdl, nameDecisionClass[encoder]), 'w')
		#############################

		##### REMOVE FEATURES THAT ARE NOT RELEVANT #####
		print "%s: Removing feats..." % (encoder)
		for idx, feat in enumerate(feats):
			if len(np.unique(trainData[:,idx])) == 1:
				if feat not in featuresToRemove:
					featuresToRemove += [feat]

		feats, trainData, testData = remove_feats(feats, featuresToRemove, trainData, testData)
		#################################################

		##### GET ORIGINAL NAMES OF THE FEATURES (as they are originally in the encoders, for compatibility) #####
		###################### ALSO READS SIZES OF EACH FEATURE FROM ALTERNATIVE FILE ############################
		############# HM AND X265 CODE CANNOT HAVE COMMENTS IN THE SAME LINE AS VARIABLE IN .H ###################
		print "%s: Getting features sizes..." % (encoder)
		featsNamed[encoder] = fill_feats_named(feats, encoder, nameDefaultClass[encoder], reqHeaderToVar[encoder])
		##########################################################################################################

		##### GET X AND y VECTORS FROM THE TRAIN AND TEST DATA #####
		XTrain, yTrain = trainData[:,:-1] , trainData[:,-1]

		a, counts = np.unique(yTrain, return_counts = True)

		XTest, yTest = testData[:,:-1] , testData[:,-1]
		############################################################

		##### RUN THE DECISION TREE ALGORITHM (OR AN ALTERNATIVE ONE, LIKE THE RANDOM FOREST) #####
		print "%s: Running classifier..." % (encoder)
		clf = None
		if classifier == 'tree':
			clf = DecisionTreeClassifier(max_depth = maxDepth).fit(XTrain,yTrain) #max_depth = 10
		elif classifier == 'forest':
			clf = RandomForestClassifier(max_depth = maxDepth, n_estimators = numTrees).fit(XTrain,yTrain)
		#print cross_val_score(clf, XTrain, yTrain, cv = 5)
		###########################################################################################

		##### TEST THE TREE USING THE TEST DATA PREVIOUSLY OBTAINED AND OBTAIN A HIT PERCENTAGE #####
		print "%s: Checking hit percentage using test data..." % (encoder)
		yPredict = clf.predict(XTest)
		hitValues.append((yPredict == yTest).mean())
		print "hit: %f" % ((yPredict == yTest).mean())
		#############################################################################################

		##### GENERATE THE DECISION TREE IN PDF FORMAT (IMAGE), IN A .CPP AND .H CLASS (FOR THE ENCODERS), AND IN A .VHD FORMAT (TO USE WITH THE IME) #####
		print "%s: Generating PDF..." % (encoder)
		plot_PDF(clf, encoder, classifier=classifier, path='%s' % (dirPdf[classifier]), feats=feats, numTrees=numTrees)
		print "%s: Generating classifier class..." % (encoder)
		gen_class(clf, classifier, featsNamed[encoder], fileOutH, fileOutCpp, nameCommonDir[encoder], nameDefaultClass[encoder], nameFunction, nameDecisionClass[encoder], numTrees)
		print "%s: Generating VHDL file..." % (encoder)
		tree_to_vhdl(clf, classifier, featsNamed[encoder], fileOutVhdl, numTrees)
		###################################################################################################################################################

		#print_corrs(feats, X, y)

		##### CLOSE FILES AND MOVE RESPECTIVE FILES TO THEIR DESTINY DIRECTORIES #####
		fileOutCpp.close()
		fileOutH.close()
		fileOutVhdl.close()
		os.system('mv %s.cpp %s/' % (nameDecisionClass[encoder], dstDirEncoder[encoder]))
		os.system('mv %s.h %s/' % (nameDecisionClass[encoder], dstDirEncoder[encoder]))
		os.system('cp %s/* %s/' % (dirVhdl, dstDirVhdl))
		##############################################################################

	return hitValues




fOut = open("hit_values.csv", "w")
numTrees = range(2,10)
nrSimuls = range(3)
classifiers = ['tree', 'forest']
maxDepths = range(2,10)

maxDepths = range(2,3)
numTrees = range(2,3)
classifiers = ['tree']

print >> fOut, ",".join(['Encoder', 'Classifier', 'MaxDepth', 'Simul', 'NumTrees', 'Hit'])

for classifier in classifiers:
	if classifier == 'tree':
		for maxDepth in maxDepths:
			hitValues = train_data(classifier, 1, maxDepth)
			for idx, encoder in enumerate(encoders):
				print >> fOut, ",".join([str(x) for x in [encoder, classifier, maxDepth, '0', '1', hitValues[idx]]])
	elif classifier == 'forest':
		for nrSimul, numTree, maxDepth in product(nrSimuls, numTrees, maxDepths):
			hitValues = train_data(classifier, numTree, maxDepth)
			for idx, encoder in enumerate(encoders):
				print >> fOut, ",".join([str(x) for x in [encoder, classifier, maxDepth, nrSimul, numTree, hitValues[idx]]])

fOut.close()
